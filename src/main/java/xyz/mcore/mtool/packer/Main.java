/*
 * Copyright (C) 2016-2022 Ozan Egitmen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.mcore.mtool.packer;

import io.minio.MinioClient;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

@SuppressWarnings({"ResultOfMethodCallIgnored", "DuplicateExpressions"})
public class Main {

    public static void main(String[] args) throws Exception {
        var startTime = System.currentTimeMillis();
        var currentDir = System.getProperty("user.dir").replace('\\', '/').toLowerCase() + "/";

        for (var file : new String[]{"7za.exe", "ext_asst.exe", "packer.properties", "task_force_radio.ts3_plugin"}) {
            if (!new File(currentDir + file).exists()) {
                System.err.println(file + " missing!");
                System.exit(1);
            }
        }

        var properties = new Properties();
        var fis = new FileInputStream("packer.properties");
        properties.load(fis);
        fis.close();
        properties.setProperty("packer.target", properties.getProperty("packer.target").replace('\\', '/').toLowerCase());

        var minioClient = new MinioClient(
                properties.getProperty("minio.endpoint"),
                properties.getProperty("minio.accessKey"),
                properties.getProperty("minio.secretKey")
        );

        var bucketName = properties.getProperty("minio.bucketName");
        if (!minioClient.bucketExists(bucketName)) {
            minioClient.makeBucket(bucketName);
            minioClient.setBucketPolicy(bucketName, "{\n" +
                    "  \"Version\":\"2012-10-17\",\n" +
                    "  \"Statement\":[\n" +
                    "    {\n" +
                    "      \"Sid\":\"AddPerm\",\n" +
                    "      \"Effect\":\"Allow\",\n" +
                    "      \"Principal\": \"*\",\n" +
                    "      \"Action\":[\"s3:GetObject\"],\n" +
                    "      \"Resource\":[\"arn:aws:s3:::" + bucketName + "/*\"]\n" +
                    "    }\n" +
                    "  ]\n" +
                    "}");
        }

        var database = new File(properties.getProperty("packer.target") + "/modfiles.dbzip");

        var head = new ArrayList<ModFile>();
        if (database.exists() && database.length() > 15) {
            var in = new InflaterInputStream(new FileInputStream(database));
            var baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[4096];
            int len;
            while ((len = in.read(buffer)) > 0) {
                baos.write(buffer, 0, len);
            }
            in.close();
            String line;
            var br = new BufferedReader(new StringReader(baos.toString()));
            while ((line = br.readLine()) != null && !line.startsWith("Packing")) {
                head.add(new ModFile(line));
            }
            br.close();
            Files.write(Paths.get(database + ".old"), Files.readAllBytes(database.toPath()));
        } else {
            minioClient.listObjects(bucketName).forEach(object -> {
                try {
                    minioClient.removeObject(bucketName, object.get().objectName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }

        Files.write(database.toPath(), new byte[]{120, -100, 75, -54, 44, 0, 0, 2, 107, 1, 60}); // compressed "bip"

        System.out.println("Getting hashes...");
        //noinspection resource
        var localPaths = Files.walk(new File(currentDir + "path").toPath()).filter(path -> {
            // Delete old files from aborted packing operations
            if (path.toString().endsWith(".chash") || path.toString().endsWith(".7z")) {
                try {
                    Files.delete(path);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return false;
            }
            return !path.toFile().isDirectory();
        }).toList();

        var mappingJobs = Executors.newFixedThreadPool(6);
        var local = new CopyOnWriteArrayList<ModFile>();

        for (int i = 0; i < localPaths.size(); i++) {
            final int index = i;

            mappingJobs.submit(() -> {
                var path = localPaths.get(index);
                var modFile = new ModFile(path.toFile());

                try {
                    var hasher = new ProcessBuilder(currentDir + "ext_asst.exe", path.toString()).start();
                    var br = new BufferedReader(new InputStreamReader(hasher.getInputStream()));
                    modFile.hash = br.readLine();
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                local.add(modFile);
            });
        }

        mappingJobs.shutdown();
        if (!mappingJobs.isTerminated()) {
            mappingJobs.awaitTermination(1, java.util.concurrent.TimeUnit.MINUTES);
        }

        var sb = new StringBuffer();
        int skipCount = 0, deleteCount = 0;
        for (var headFile : head) {
            var matchFound = false;

            for (var localFile : local) {
                if (localFile.remove || !headFile.name.equalsIgnoreCase(localFile.name)) {
                    continue;
                }
                matchFound = true;

                if (headFile.hash.equals(localFile.hash)) {
                    new File(currentDir + "path/" + localFile.name + ".chash").delete();
                    sb.append(headFile);
                    localFile.remove = true;
                    System.out.println("SKIP " + localFile.name);
                    skipCount++;
                }
                break;
            }

            if (!matchFound) {
                System.out.println("DEL " + headFile.name);
                minioClient.removeObject(bucketName, headFile.name + ".7z");
                deleteCount++;
            }
        }
        local.removeIf(localFile -> localFile.remove);

        var compressJobs = Executors.newFixedThreadPool(Integer.parseInt(properties.getProperty("packer.compressThreads")));
        var uploadJobs = Executors.newFixedThreadPool(Integer.parseInt(properties.getProperty("packer.uploadThreads")));

        var packTime = System.currentTimeMillis();
        local.forEach(localFile -> compressJobs.submit(() -> {
            var name = localFile.name;
            System.out.println("PACK " + name);
            var fileName = currentDir + "path/" + name;
            var hashFile = name.endsWith(".pbo") ? fileName + ".chash" : "";

            try {
                var sevenZipBuilder = new ProcessBuilder(currentDir + "7za.exe", "a", "-mx9", "-aoa", "-pGk6pu2W4cPJ7V5CSYv4p", fileName + ".7z", fileName);

                if (!hashFile.isEmpty()) {
                    sevenZipBuilder.command().add(hashFile);
                }

                var sevenZip = sevenZipBuilder.start();
                sevenZip.waitFor();
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }

            if (!hashFile.isEmpty()) {
                new File(hashFile).delete();
            }

            var file = new File(fileName + ".7z");
            localFile.compressedSize = file.length();
            localFile.packTime = packTime;

            uploadJobs.submit(() -> {
                System.out.println("UPLOAD " + name);

                try {
                    minioClient.putObject(bucketName, name + ".7z", fileName + ".7z", null);
                    localFile.url = minioClient.getObjectUrl(bucketName, name + ".7z");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                sb.append(localFile);
                file.delete();
            });
        }));

        compressJobs.shutdown();
        if (!compressJobs.isTerminated()) {
            compressJobs.awaitTermination(Integer.parseInt(properties.getProperty("packer.compressTimeout")), TimeUnit.MINUTES);

            uploadJobs.shutdown();
            if (!uploadJobs.isTerminated()) {
                uploadJobs.awaitTermination(Integer.parseInt(properties.getProperty("packer.uploadTimeout")), TimeUnit.MINUTES);
            }
        }

        minioClient.putObject(
                bucketName,
                "task_force_radio.ts3_plugin",
                currentDir + "task_force_radio.ts3_plugin",
                null
        );
        var url = minioClient.getObjectUrl(bucketName, "task_force_radio.ts3_plugin");

        var message = String.format(
                "Packing complete in %s, packed %d files, skipped %d, deleted %d (%s) @%d",
                LocalTime.MIN.plusSeconds((System.currentTimeMillis() - startTime) / 1000).toString(),
                local.size(), skipCount, deleteCount, url, packTime
        );
        sb.append(message);

        var baos = new ByteArrayOutputStream();
        var out = new DeflaterOutputStream(baos);
        out.write(sb.toString().getBytes());
        out.close();

        Files.write(database.toPath(), baos.toByteArray());
        Files.deleteIfExists(Paths.get(database + ".old"));
        System.out.println(message);
    }
}
