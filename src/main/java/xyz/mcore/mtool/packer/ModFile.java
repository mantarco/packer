/*
 * Copyright (C) 2016-2022 Ozan Egitmen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.mcore.mtool.packer;

import java.io.File;

class ModFile {

    final String name; // File itself
    String hash; // For comparison
    String url; // For downloading the file
    private final int size; // For light comparison & integrity validation
    long compressedSize; // Used for calculating download size
    long packTime; // Used in comparison to see if getting a hash is worth it
    boolean remove = false;

    ModFile(File f) {
        var sanitized = f.getPath().replace('\\', '/').toLowerCase();
        name = sanitized.substring(sanitized.indexOf("/path/") + 6);
        size = (int) f.length();
    }

    ModFile(String e) {
        name = e.substring(0, e.indexOf('|'));
        size = Integer.parseInt(e.substring(e.indexOf('|') + 1, e.indexOf('>')));
        compressedSize = Long.parseLong(e.substring(e.indexOf('>') + 1, e.indexOf(':')));
        hash = e.substring(e.indexOf(':') + 1, e.indexOf('?'));
        packTime = Long.parseLong(e.substring(e.indexOf('?') + 1, e.indexOf('*')));
        url = e.substring(e.indexOf('*') + 1);
    }

    @Override
    public String toString() {
        return String.format("%s|%d>%d:%s?%d*%s\n", name, size, compressedSize, hash, packTime, url);
    }
}
